build: ks/kuklinux-live.ks update-boot
	ksflatten -c ks/kuklinux-live.ks -o tmp/build.ks
	sudo livemedia-creator \
		--iso-name=kuklinux.iso \
		--iso-only \
		--iso=./artefactos/boot.iso \
		--make-iso \
		--project='Kuk GNU & Linux' \
		--releasever=1 \
		--resultdir=result \
		--tmp=tmp \
		--ks=tmp/build.ks

clean:
	rm -f *.log
	rm -f tmp/build.ks
	sudo rm -fr result

sync-web:
	rsync -avP --delete web/ web1.kuklinux.dev:/srv/www/html/kuklinux.dev/default/public/

test: result/kuklinux.iso
	qemu-system-x86_64 \
		-machine accel=kvm \
		-smp 4 \
		-m 4096 \
		-vga qxl \
		-cdrom \
		result/kuklinux.iso

update-boot: artefactos/boot.iso
	wget -c -O artefactos/boot.iso http://mirrors.syringanetworks.net/fedora/linux/releases/32/Everything/x86_64/os/images/boot.iso

