%include kuklinux-base.ks

## crystal
repo --name=crystal --baseurl=https://download.copr.fedorainfracloud.org/results/zawertun/crystal/fedora-$releasever-$basearch/

# budgie
repo --name=budgie --baseurl=https://download.copr.fedorainfracloud.org/results/alunux/budgie-desktop-git/fedora-$releasever-$basearch/


%packages

# dev
@development-tools
crystal
shards

# desktop environments
arc-icon-theme
budgie-brightness-control-applet
budgie-desktop
budgie-haste-applet
budgie-pixel-saver-applet
budgie-vala-panel-appmenu-plugin
gnome-terminal
moka-icon-theme

%end


%post
# add repos
dnf -y copr enable alunux/budgie-desktop-git
dnf -y copr enable zawertun/crystal

# set background
## get it
curl https://downloads.woralelandia.com/bg/kgl.png > /usr/share/backgrounds/default.png

## set dconf
cat << EOF > /etc/dconf/db/local.d/00-background
[org/gnome/desktop/background]
picture-uri='file:///usr/share/backgrounds/default.png'
picture-options='stretched'

EOF

## add locks
cat << EOF > /etc/dconf/db/local.d/locks/background
# List the keys used to configure the desktop background
/org/gnome/desktop/background/picture-uri
/org/gnome/desktop/background/picture-options

EOF

# budgie
cat << EOF > /etc/dconf/db/local.d/00-budgie
[org/gnome/desktop/interface]
gtk-theme='Adapta-Nokto-Eta'
icon-theme='Moka'
EOF

# update dconf
dconf update

%end
